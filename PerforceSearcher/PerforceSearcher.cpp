// PerforceSearcher.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>

#include "clientapi.h"

int main()
{
    ClientUser ui;
    ClientApi client;
    StrBuf msg;
    Error e;

    // Any special protocol mods

    // client.SetProtocol( "tag" );

    // Enable client-side Extensions
    // client.EnableExtensions();

    // Connect to server

    client.Init(&e);

    if (e.Test())
    {
        e.Fmt(&msg);
        fprintf(stderr, "%s\n", msg.Text());
        return 1;
    }

    //client.SetArgv(argc - 2, argv + 2);
    //client.Run(argv[1], &ui);

    // Close connection

    client.Final(&e);

    if (e.Test())
    {
        e.Fmt(&msg);
        fprintf(stderr, "%s\n", msg.Text());
        return 1;
    }

    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
